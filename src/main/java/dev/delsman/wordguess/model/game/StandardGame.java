package dev.delsman.wordguess.model.game;

import dev.delsman.wordguess.model.Word;
import dev.delsman.wordguess.view.GameView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StandardGame extends Game {

    // TODO: load words from a CSV or JSON file
    private List<Word> wordList = new ArrayList<>(
            Arrays.asList( new Word("hoofddorp", "city in the Netherlands", 1),
                    new Word("young", "not old", 2),
                    new Word("capital", "not little", 3),
                    new Word("java", "surrounded by sea", 4),
                    new Word("trainee", "still learning", 5)
            )
    );

    // keeps track of the words
    private int wordCounter = 0;

    private GameView gameView = new GameView("STANDARD GAME");

    @Override
    public void prepareGame() {

        gameView.generateText();
    }

    @Override
    public void startGame() {

        gameView.askForInput();
        // start the time
        start = System.currentTimeMillis();
        ongoing = true;
    }

    @Override
    public void handleInput() {

        // while game is still going
        while(ongoing) {
            // if there are words left
            if(wordCounter < wordList.size()) {
                // ask view to return points
                int points = gameView.askForGuess(wordList.get(wordCounter));
                score += points;
                wordCounter++;
            }
            else {
                ongoing = false;
            }
        }
    }

    @Override
    public void gameEnded() {

        // stop the time
        finish = System.currentTimeMillis();

        long timeElapsed = finish - start;

        // deduct elapsed time from score
        score -= (timeElapsed / 1000); // divide millis by 1000 to create seconds
        if(score < 0) score = 0; // if score is lower than 0, make it a 0

        gameView.gameEnded(score);
    }
}
