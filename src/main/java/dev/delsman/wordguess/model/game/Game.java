package dev.delsman.wordguess.model.game;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Getter
public abstract class Game {

    protected BufferedReader BUFFERED_READER = new BufferedReader(new InputStreamReader(System.in));

    // used to measure game-time
    protected long start, finish;

    protected int score;

    protected boolean ongoing;

    public abstract void prepareGame();

    public abstract void startGame();

    public abstract void handleInput();

    public abstract void gameEnded();
}
