package dev.delsman.wordguess.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Word {

    private String word;

    private String tip;

    private String scrambledWord;

    private int position;

    public Word(String word, String tip, int position) {
        this.word = word;
        this.tip = tip;
        this.position = position;

        // make scrambledWord equal to word
        this.scrambledWord = word;
        // scramble scrambledWord until it's different from word
        while(scrambledWord.equals(word)) {
            this.scrambledWord = scramble(word);
        }
    }

    private String scramble(String input) {

        String output;
        List<Character> characterList = new ArrayList<>();

        // for each character (letter) in input
        for(char c : input.toCharArray()) {
            characterList.add(c);
        }

        // create a new string builder to generate our scrambled word
        StringBuilder stringBuilder = new StringBuilder(input.length());

        while(characterList.size() > 0) {
            // pick a random char from the list
            int randPicker = (int)(Math.random() * characterList.size());
            // remove the char from list and append it to the string builder
            stringBuilder.append(characterList.remove(randPicker));
        }

        output = stringBuilder.toString();

        return output;
    }
}
