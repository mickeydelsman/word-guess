package dev.delsman.wordguess.view;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public interface View {

    BufferedReader BUFFERED_READER = new BufferedReader(new InputStreamReader(System.in));

    void generateText();
}
