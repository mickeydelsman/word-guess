package dev.delsman.wordguess.view;

import org.springframework.stereotype.Component;

@Component
public class StartView implements View {

    @Override
    public void generateText() {
        // generate welcome text
        System.out.println("Welcome to Word Guess! \n");
        System.out.println("This game is developed as a assignment for YoungCapital. \n");
        System.out.println("Enter '1' to start a standard game. \n" +
                "Enter '2' to create a custom game (under development). \n" +
                "Enter '3' to add new words to the game (under development)."
        );
    }

    public int askForInput() {

        int choice = 0;
        int input;

        while(choice == 0) {

            System.out.print("\nPlease make your choice (Enter 1,2 or 3): ");
            // try to read input
            try { input = Integer.parseInt(BUFFERED_READER.readLine()); }
            // use a normal exception instead of a IOException (so it doesn't crash the game)
            catch (Exception e){
                System.out.println("\nSorry, I do not understand that");
                // go to next iteration
                continue;
            }
            // make choice according to input
            if(input == 1)
                choice = input;
            else if(input == 2 || input == 3) {
                System.out.println("\nSorry, it's still being developed.");
            }
            else {
                System.out.println("\nThat's not a valid option.");
            }
        }

        return choice;
    }
}
