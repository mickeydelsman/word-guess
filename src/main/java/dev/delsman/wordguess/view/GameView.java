package dev.delsman.wordguess.view;

import dev.delsman.wordguess.model.Word;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class GameView implements View {

    private String gameMode;

    public GameView(String gameMode) {

        this.gameMode = gameMode;
    }

    @Override
    public void generateText() {

        System.out.println("\n" + gameMode + "\n");
        System.out.println("The goal of this game is to guess all the scrambled words correctly.\n" +
                "If you do not know the answer you can ask for a hint (just type 'hint' as your guess). \n" +
                "Hints will cost you points, so be careful.\n" +
                "If you do not know the answer at all, you can skip it (type 'skip' as your guess) this will grant you 0 points. \n" +
                "When you guessed (or skipped) all the word's correctly, the timer will stop and your score will be calculated.\n");
    }

    public void askForInput() {

        String input = "";

        System.out.print("When your ready, type 'start' to begin: ");

        while (!input.equals("start")) {
            try {
                input = BUFFERED_READER.readLine().toLowerCase();
                if (input.equals("start")) {
                    System.out.println("\nAlright, lets go! \n");
                    // dramatic pause for 1 second (build up some excitement)
                    TimeUnit.SECONDS.sleep(1);
                }
                else {
                    System.out.println("\nThat's not correct\n");
                    System.out.print("please try again: ");
                }
            }
            catch (Exception e) { System.out.println("Something went wrong: " + e); }
        }
    }

    public int askForGuess(Word word) {

        // TODO: store points as a member
        int points = 20;
        String guess = "";

        System.out.println("Word number "+ word.getPosition() + " is '" + word.getScrambledWord() + "'.");

        while(!guess.equals(word.getWord())) {

            System.out.print("\nYour guess: ");

            // try to read input
            try { guess = BUFFERED_READER.readLine().toLowerCase(); }
            catch (IOException e) { System.out.println("Something went wrong handling the input: " + e); }

            if(guess.equals(word.getWord())) {
                System.out.println("\nThat's correct! \n");
            }
            else if (guess.equals("hint")) {
                System.out.println("\nOke your hint is '" + word.getTip() + "'.");
                // deduct 10 points for hint
                points -= 10;
            }
            else if (guess.equals("skip")) {
                System.out.println("\nYou skipped word " + word.getPosition() + ".\n");
                // skip word by returning 0
                return 0;
            }
            else {
                System.out.println("\nI'm afraid that's not correct, try again. \n");
                System.out.println("Your word is: '" + word.getScrambledWord() + "'");
                // deduct 5 point for bad answer
                points -= 5;
            }
        }

        return points;
    }

    public void gameEnded(long score) {

        // TODO: store max score as a member (words * score)
        System.out.println("Your have a score of '" + score + "' out of '100'.\n");
        System.out.println("Game over, thank you for playing!");
    }
}