package dev.delsman.wordguess;

import dev.delsman.wordguess.model.game.Game;
import dev.delsman.wordguess.model.game.StandardGame;
import dev.delsman.wordguess.view.StartView;
import org.springframework.stereotype.Component;

@Component
public class Start {

    private Game game;

    private StartView startView = new StartView();

    public Start() {

        // ask view to welcome player
        startView.generateText();

        // ask view for input
        int gameChoice = startView.askForInput();

        if (gameChoice == 1)
            game = new StandardGame();

        // ask game to prepare itself
        game.prepareGame();

        // ask game to start and handle input
        game.startGame();
        game.handleInput();

        // end game
        game.gameEnded();
    }
}
